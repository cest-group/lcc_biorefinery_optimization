# AI-guided biorefinery optimization 
Repository for all code and data related to the paper *AI-guided biorefinery optimization for the production of lignin-carbohydrate complexes with tailored properties*.

If you use the provided code or data in your research please cite
- *AI-guided biorefinery optimization for the production of lignin-carbohydrate-complexes with tailored properties*, D. Diment *et al*. (2024)

## Installation
The paper utilizes simple wrappers for BOSS to perform the model fitting and acquistions, the resulting code has been packaged into a small Python library that can be installed according to the instructions below. This will also install the required dependencies, i.e., BOSS, and we recommend to run the commands inside a virtual environment.

```bash
git clone https://gitlab.com/cest-group/lcc_biorefinery_optimization.git
cd lcc_biorefinery_optimization
python3 -m pip install .
```
## Usage
```python
from lccopt.visualize import plot_box_contours
import matplotlib.pyplot as plt

# Choose target to plot, available options include: ael, carbs, tg, surf025, bo4.
# See table in README for descriptions of all available options.
# As an example let's plot the acetone-extracted lignin yield
target = "ael"

# Visualize the lignin yield surrogate model for batches 1-7
for max_batch in range(1, 8):
    plot_box_contours(target, view="auto", max_batch=max_batch)
    plt.show()
```

## Data
The data set consists of initial Sobol points, BO acquistions and test points and is distributed as a CSV file ``data/data_master.csv``. The data is organized in a table where 72 data points are stored row-wise with the following columns: 

| Column label | Description | Unit |
| ---      | ---      | ---      |
| ID   | Unique sample identifier   |    |
| pfac | Reaction severity |  |
| temp   | Reactor temperature | Celsius  |
| lsr | Reactor liquid-to-solid ratio |  |
| ael | Acetone-extracted lignin yield | % |
| carbs | Total carbohydrate content | occurence per 100 Ar |
| target | GP model used to generate sample |  |
| acqfn | Acquisition function used to generate sample  |  |
| batch | Batch number of sample |  |
| rsi | Radical scavening index   | mmol/g |
| surf05 | Surface tension at 0.5 mg/mL | mN/n |
| surf04 | Surface tension at 0.4 mg/mL | mN/n |
| surf025 |  Surface tension at 0.25 mg/mL | mN/m |
| surf01 | Surface tension at 0.1 mg/mL | mN/m |
| surf008 | Surface tension at 0.08 mg/mL | mN/m |
| bo4 | $\beta$-O-4 linkages | occurence per 100 Ar  |
| chain | Length of carbohydrate chains | |


### Raw data vs. BO data
The BO data set was distilled from a larger raw set of measurements coming from a total of 90 samples: ``data/sample_master.csv``. In addition to measurements for the original 72 samples, the sample set contains measurements for 18 replicates. These were created for the purpose of measuring physicochemical properties when the amount of lignin in the original sample was not enough. The correspondence between the BO data and the sample measurements is described by the table below. Here, the leftmost column __data point__ indicates an entry in the ``data_master.csv`` file and 
the __operation__ column indicates how the data point is obtain from the corresponding entries in the ``sample_master.csv`` file.
The __average__ operation means that the __data point__ was obtained by averaging numerical values for the measured properties of __sample__ and __replicate__.
The __equal__ operation means that the __data point__ is the same as __sample__, with missing values added from __replicate__. The table only lists sample codes for which information from replicates was used.

| data point | operation | sample | replicate |
| ---      | ---      | ---      | ---      |
|P813-169-1.56 | average | P813-169-1.56 | P813-169-1.56-II | 
|P906-191-0.47| average |P906-191-0.47|P906-191-0.47-II| 
|P719-164-0.9 | average | P719-164-0.9 | P719-164-0.9-II | 
|P344-182-1.78 | average | P344-182-1.78 | P344-182-1.78-II | 
|P391-171-0.8 | average | P391-171-0.8 | P391-171-0.8-II | 
|P766-188-1.67 | average | P766-188-1.67 | P766-188-1.67-II | 
|P953-162-1.23 | average | P953-162-1.23 | P953-162-1.23-II | 
|P578-180-0.36 | average | P578-180-0.36 | P578-180-0.36-II | 
|P625-178-1.13 | equal | P625-178-1.13-II | P625-178-1.13 | 
|P438-186-0.69| equal |P438-186-0.69-II|P438-186-0.69| 
|P531-173-1.34| equal |P531-173-1.34-II|P531-173-1.34| 
|P297-193-1.45| equal |P297-193-1.45|P297-193-1.45-R|
|P281-164-0.50| equal |P281-164-0.50|P281-164-0.50-R|
|P250-183-0.40| equal |P250-183-0.40|P250-183-0.40-R|
|P414-190-0.63| equal |P414-190-0.63|P414-190-0.63-R|
|P265-192-2.00| equal |P265-192-2.00|P265-192-2.00-R|
|P728-160-0.25| equal |P728-160-0.25|P728-160-0.25-R|
|P1000-195-0.25| equal |P1000-195-0.25|P1000-195-0.25-R|

Furthermore, due to a labeling issue, the AEL yield and carbohydrate content for sample __P850-182-1.23__ were incorrectly given as 14% and 3.3 per 100 Ar during theoriginal BO process. The csv file included in this repository contains the corrected values.

from lccopt.visualize import plot_box_contours
import matplotlib.pyplot as plt

# Choose target to plot, available options include: ael, carbs, tg, surf025, bo4.
# See table in README for descriptions of all available options.
# As an example let's plot the acetone-extracted lignin yield
target = "ael"

# Visualize the lignin yield surrogate model for batches 1-7
for max_batch in range(1, 8):
    plot_box_contours(target, view="auto", max_batch=max_batch)
    plt.show()

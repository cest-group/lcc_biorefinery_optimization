from __future__ import annotations

from collections.abc import Sequence
from dataclasses import dataclass

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import numpy.typing as npt
from matplotlib.colorbar import Colorbar
from matplotlib.figure import Figure

from lccopt.analyze import calc_pareto, calc_score
from lccopt.data import INFO
from lccopt.doe import DOE
from lccopt.utils import grid_calc


@dataclass
class PlotObjects:
    """Dataclass for storing plot objects"""
    ax: plt.Axes | None = None
    fig: Figure | None = None
    ax_arr: np.ndarray | None = None
    cbar: Colorbar | None = None


def prettify_3d(ax=None, hide_panes=True, hide_grid=False):
    if ax is None:
        ax = plt.gca()
    ax.tick_params(pad=-3, axis="x")
    ax.tick_params(pad=-3, axis="y")
    ax.tick_params(pad=-2, axis="z")

    ax.set_xticklabels(
        ax.get_xticklabels(),
        rotation=0,
        verticalalignment="baseline",
        horizontalalignment="left",
    )
    ax.locator_params(nbins=4, axis="y")
    ax.locator_params(nbins=4, axis="z")

    if hide_panes:
        ax.xaxis.set_pane_color(color=(1, 1, 1))
        ax.yaxis.set_pane_color(color=(1, 1, 1))
        ax.zaxis.set_pane_color(color=(1, 1, 1))
    if hide_grid:
        ax.grid(visible=False)


def plot_box_contours(
    target: str,
    doe: DOE | None = None,
    ax: plt.Axes | None = None,
    view: str = "auto",
    max_batch: int = 7,
) -> PlotObjects:
    """
    Visualize a surrogate model as a 3d box of contours plots.

    Parameters
    ----------
    target : str
        The target quantity represented by the surrogate model. Options:
    doe : DOE | None
        DOE object. If None, a new one will be created.
    ax : plt.Axes | None
        Axes object to plot on. If None, a new one will be created.
    view : str
        Default viewing angle, options: 'auto', 'front', 'back'.
        Defaults to 'auto' which selects an appropriate view based on the target.
    max_batch : int
        Include data from batches up to max_batch in the model.

    Returns
    -------
    PlotObjects
        Dataclass containing matplotlib objetcs (figure, axes...)
    """
    if doe is None:
        doe = DOE()

    if ax is None:
        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    model = doe.fit_model(target, max_batch=max_batch)

    grid, F = grid_calc(
        model.predict, bounds=model.bounds, free_dims=(0, 1, 2), grid_pts=50
    )
    X, Y, Z = grid
    cont_kw = {
        "vmin": np.floor(F.min()),
        "vmax": np.ceil(F.max()),
        "levels": np.linspace(np.floor(F.min()), np.ceil(F.max()), 20),
        "cmap": "coolwarm",
        "depthshade": False,
        "alpha": 1,
    }
    edges_kw = dict(color="0.4", linewidth=1, zorder=1e3)

    xmin, xmax = doe.bounds[0]
    ymin, ymax = doe.bounds[1]
    zmin, zmax = doe.bounds[2]

    # plot from the back
    if (
        view == "auto" and any([t in target for t in ["surf", "carbs", "tg", "bo4"]])
    ) or view == "back":
        view = "back"
        _ = ax.contourf(
            X[:, :, 0], Y[:, :, 0], F[:, :, -1], zdir="z", offset=zmax, **cont_kw
        )
        _ = ax.contourf(
            X[0, :, :], F[0, :, :], Z[0, :, :], zdir="y", offset=ymin, **cont_kw
        )
        conts = ax.contourf(
            F[:, 0, :], Y[:, 0, :], Z[:, 0, :], zdir="x", offset=xmin, **cont_kw
        )
        ax.plot([xmin, xmin], [ymin, ymin], [zmin, zmax], **edges_kw)
        ax.plot([xmin, xmax], [ymin, ymin], [zmax, zmax], **edges_kw)
        ax.plot([xmin, xmin], [ymin, ymax], [zmax, zmax], **edges_kw)
        ax.view_init(21, -130, 0)
    else:  # plot from the front
        view = "front"
        _ = ax.contourf(
            X[:, :, 0], Y[:, :, 0], F[:, :, -1], zdir="z", offset=zmax, **cont_kw
        )
        _ = ax.contourf(
            X[0, :, :], F[-1, :, :], Z[0, :, :], zdir="y", offset=ymax, **cont_kw
        )
        conts = ax.contourf(
            F[:, -1, :], Y[:, 0, :], Z[:, 0, :], zdir="x", offset=xmax, **cont_kw
        )
        ax.plot([xmin, xmax], [ymax, ymax], [zmax, zmax], **edges_kw)
        ax.plot([xmax, xmax], [ymin, ymax], [zmax, zmax], **edges_kw)
        ax.plot([xmax, xmax], [ymax, ymax], [zmin, zmax], **edges_kw)
        ax.view_init(23, 48, 0)

    # Set limits of the plot from coord limits
    ax.set(xlim=[xmin, xmax], ylim=[ymin, ymax], zlim=[zmin, zmax])

    ax.xaxis.set_major_locator(mticker.MaxNLocator(4))
    prettify_3d(ax, hide_panes=True, hide_grid=True)

    # Set labels and zticks
    ax.set_xlabel(INFO["pfac"].label_with_unit)
    ax.set_ylabel(INFO["temp"].label_with_unit)
    ax.set_zlabel(INFO["lsr"].label_with_unit)

    # Colorbar
    fig = ax.get_figure()
    if view == "back":
        pad = 0.09
    else:
        pad = 0.03
    cbar = fig.colorbar(
        conts,
        ax=ax,
        fraction=0.02,
        pad=pad,
        ticks=mticker.MaxNLocator(7),
    )
    cbar.set_label(INFO[target].label_with_unit)
    plt.subplots_adjust(left=0.1, right=0.8)
    plt_objs = PlotObjects(fig=fig, ax=ax, cbar=cbar)
    return plt_objs


def plot_slice(
    target: str,
    doe: DOE | None =None,
    ax: plt.Axes | None = None,
    fix_var: str = "lsr",
    fix_val: float | str = "max",
    max_batch: int = 7,
    inc_acqs: bool= False,
    inc_max: bool= True,
    title: bool= True,
) -> PlotObjects:
    """
    Visualizes a surrogate model by plotting a 2D contour slice
    obtained by holding one of the three inputs fixed.

    Parameters
    ----------
    target : str
        The target quantity represented by the surrogate model. Options:
    doe : DOE | None
        DOE object. If None, a new one will be created.
    ax : plt.Axes | None
        Axes object to plot on. If None, a new one will be created.
    fix_var : str
        Input variable to fix, defaults to lsr = liquid-to-solid ratio.
    fix_val : float | str
        Value for the fixed input variable, defaults to max which uses the variable
        value at the global optimum location.
    max_batch : int
        Include data from batches up to max_batch in the model.
    inc_acqs : bool
        Whether to include acquisition locations in the plot.
    inc_max : bool
        Whether to include the global optimum in the plot.
    title : bool
        Whether to add a title to the plot

    Returns
    -------
    PlotObjects
        Dataclass containing matplotlib objetcs (figure, axes...)
    """
    doe = doe or DOE()
    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig = ax.get_figure()

    model = doe.fit_model(target, max_batch=max_batch)

    res = model.optimize()
    x_max = res.x

    var_to_dim = {"pfac": 0, "temp": 1, "lsr": 2}
    dim_to_var = {0: "pfac", 1: "temp", 2: "lsr"}
    fix_dim = var_to_dim[fix_var]
    free_dims = tuple(set(var_to_dim.values()) - {fix_dim})

    if fix_val == "max":
        fix_val = x_max[fix_dim]
        fix_label = f"{INFO[fix_var].label}: {fix_val: .2f} (at max)"
    else:
        fix_label = f"{INFO[fix_var]}: {fix_val:.2f}"

    grid, Y_grid = grid_calc(
        model.predict,
        doe.bounds,
        free_dims=free_dims,
        grid_pts=(50,),
        fixed_dim_vals=fix_val,
    )

    conts = ax.contourf(*grid, Y_grid, levels=20, cmap="coolwarm")
    X_acqs = model.X
    len_scale = model.get_unfixed_params()[fix_dim]

    # pick acquisitions that have the most influence for the given L/S
    if inc_acqs:
        fac = 1.0
        mask = (fix_val - fac * len_scale < X_acqs[:, 2]) & (
            X_acqs[:, 2] < fix_val + fac * len_scale
        )
        X_close = X_acqs[mask, :]
        ax.scatter(
            X_close[:, free_dims[0]],
            X_close[:, free_dims[1]],
            s=20,
            color="k",
            label="Nearby acqs.",
            clip_on=False,
        )
    if inc_max:
        ax.scatter(
            x_max[free_dims[0]],
            x_max[free_dims[1]],
            s=20,
            color="k",
            marker="x",
            label="Global maximum",
            clip_on=False,
        )

    ax.set_xlabel(INFO[dim_to_var[free_dims[0]]].label_with_unit)
    ax.set_ylabel(INFO[dim_to_var[free_dims[1]]].label_with_unit)
    if title:
        ax.set_title(fix_label)

    cbar = plt.colorbar(conts, ax=ax)
    cbar.set_label(INFO[target].label_with_unit, rotation=270, labelpad=12)
    plt_objs = PlotObjects(fig=fig, ax=ax, cbar=cbar)
    return plt_objs


def get_sample_inds(samples):
    df_test = doe.df_main[doe.df_main["target"] == "test"]
    samples_all = df_test.index.to_numpy()
    inds_all = {samp: i + 1 for i, samp in enumerate(samples_all)}
    return np.array([inds_all[s] for s in samples])


def plot_score(
    target: str,
    doe: DOE | None = None,
    datasets: Sequence = ("test",),
    verbose: bool = True,
    max_batch: int | str = 7,
    color_target: str | None = None,
):
    """
    Calculate validation metrics for a surrogate model and
    make a scatter plot of predictions vs observations for the
    training or test data set.

    Parameters
    ----------
    target : str
        The target quantity represented by the surrogate model. Options:
    doe : DOE | None
        DOE object. If None, a new one will be created.
    datasets : Sequence
        A sequence of datasets to make scatter plots for, available
        datasets are "train" and "test".
    verbose : bool
        Print information about calculated metrics.
    max_batch : int
        Include data from batches up to max_batch in the model.
    color_target : str | None
        Color-code the scattered data based on this target.

    Returns
    -------
    PlotObjects
        Dataclass containing matplotlib objetcs (figure, axes...)
    """
    doe = doe or DOE()
    n_cols = len(datasets)
    fig, ax_arr = plt.subplots(1, n_cols)
    ax_arr = np.atleast_1d(ax_arr)
    cbar = None

    for dataset, ax in zip(datasets, ax_arr):
        metrics = calc_score(
            target=target, doe=doe, dataset=dataset, max_batch=max_batch
        )
        if verbose:
            print(f"==== {target}: {dataset} ====\n")
            metrics.summarize()

        # plot predictions with uncertainties
        y_pred = metrics.y_pred
        y_std = metrics.y_std
        y_true = metrics.y_true

        i_sort = np.argsort(y_true)
        y_true = y_true[i_sort]
        y_pred = y_pred[i_sort]
        y_std = y_std[i_sort]
        X = metrics.X[i_sort, :]

        if color_target is not None:
            if color_target == "sample":
                if dataset == "train":
                    raise ValueError("Cannot color by train sample")

                color_vals = get_sample_inds(metrics.samples)
            else:
                model_color = doe.fit_model(color_target, max_batch=max_batch)
                color_vals = model_color.predict(X)

            sc = ax.scatter(
                y_true,
                y_pred,
                s=30,
                c=color_vals,
                marker="o",
                label="Model prediction",
                zorder=20,
            )
            if color_target == "sample":
                cbar = fig.colorbar(sc, ax=ax, label="Test sample number")
            else:
                cbar = fig.colorbar(sc, ax=ax, label=INFO[color_target].label_with_unit)
        else:
            sc = ax.scatter(
                y_true,
                y_pred,
                s=30,
                marker="o",
                color="tab:blue",
                label="Model prediction",
                zorder=20,
            )

        # fac = 1.96
        fac = 1.0
        ax.fill_between(
            y_true,
            y_pred - fac * y_std,
            y_pred + fac * y_std,
            color="grey",
            label=r"$68\%$ confidence",
            zorder=10,
        )

        i_min = np.argmin(y_pred)
        i_max = np.argmax(y_pred)
        i_min_t = np.argmin(y_true)
        i_max_t = np.argmax(y_true)
        y_min = min(
            y_pred[i_min] - y_std[i_min],
            y_true[i_min_t] * 0.99,
        )
        y_max = max(
            y_pred[i_max] + y_std[i_max],
            y_true[i_max_t] * 1.01,
        )
        if target == "ael":
            y_min, y_max = 2.0, 18.0

        ax.plot(
            [y_min, y_max],
            [y_min, y_max],
            ls="--",
            color="k",
            label="Perfect prediction",
            zorder=15,
        )

        ax.set_xlim([y_min, y_max])
        ax.set_ylim([y_min, y_max])
        x_label = f"{INFO[target].label_with_unit}"
        x_label = "Measured " + x_label[0].lower() + x_label[1:]
        y_label = f"{INFO[target].label_with_unit}"
        y_label = "Predicted " + y_label[0].lower() + y_label[1:]
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)

    if len(ax_arr) == 2:
        plt.subplots_adjust(wspace=0.3)
        ax_arr[0].text(0.05, 0.95, datasets[0], transform=ax_arr[0].transAxes)
        ax_arr[1].text(0.05, 0.95, datasets[1], transform=ax_arr[1].transAxes)

    ax_arr[0].legend()
    plt_objs = PlotObjects(fig=fig, ax=ax_arr, cbar=cbar)
    return plt_objs


def plot_pareto(
    targets: Sequence[str],
    doe: DOE | None = None,
    inc_acqs: bool = True,
    maximize: Sequence[bool] | None = None,
    max_batch: int | str = 7,
    grid_pts: int = 30,
) -> tuple[PlotObjects, PlotObjects]:
    """
    Performs a Pareto front analysis for a given pair of targets.
    Plot the Pareto front and feasible space in the output space
    as well as the pareto optimal solutions in input/design space.

    Parameters
    ----------
    targets : Sequence[str]
        Pair of targets for which to perform the Pareto front analysis.
    doe : DOE | None
        DOE object. If None, a new one will be created.
    inc_acqs : bool
        Whether to include a scatter plot of all acquisitions in the
        output space plot.
    maximize : Sequence[bool] | None
        Sequence of bools that indicate if the corresponding target
        should be maximized (True) or minimized (False). If None,
        both targets are maximized.
    max_batch : int
        Include data from batches up to max_batch in the model.
    grid_pts : int
        Number of grid points per axis used when calculating the Pareto front.

    Returns
    -------
    tuple[PlotObjects, PlotObjects]
        Matplotlib objetcs (figure, axes...) for the output space 
        and input space plots.
    """
    # load/fit models and calculate pareto front
    doe = doe or DOE()
    models = []
    for target in targets:
        model = doe.fit_model(target, max_batch=max_batch)
        models.append(model)

    funcs = [m.predict for m in models]
    if maximize is None:
        maximize = [m.maximize for m in models]

    X_pareto, Y_pareto, Y_feas = calc_pareto(
        funcs,
        doe.bounds,
        maximize=maximize,
        grid_pts=grid_pts,
    )

    # draw objective space figure
    fig, ax = plt.subplots()

    sc = ax.scatter(
        Y_feas[:, 0],
        Y_feas[:, 1],
        s=30,
        alpha=1.0,
        marker=".",
        color="tab:blue",
        label="Feasible point",
        clip_on=False,
    )

    sc = ax.scatter(
        Y_pareto[:, 0],
        Y_pareto[:, 1],
        s=40,
        alpha=1.0,
        marker=".",
        label="Pareto point",
        clip_on=False,
        color="tab:red",
    )

    # scatter measured values
    i_small = 0
    if len(models[0].X) > len(models[1].X):
        i_small = 1

    if inc_acqs:
        df_batch, mask = doe._filter_dataframe(targets[i_small], max_batch=max_batch)
        Y1 = df_batch.loc[mask, targets[0]].to_numpy()
        Y2 = df_batch.loc[mask, targets[1]].to_numpy()
        ax.scatter(Y1, Y2, s=20, marker="x", color="k", label="Acquisition")

    ax.set(
        ylabel=INFO[targets[1]].label_with_unit, xlabel=INFO[targets[0]].label_with_unit
    )
    ax.legend()
    fig1 = fig
    ax1 = ax

    # draw design space figure
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    sc = ax.scatter3D(
        X_pareto[:, 0],
        X_pareto[:, 1],
        X_pareto[:, 2],
        s=40,
        alpha=1.0,
        marker=".",
        color="tab:red",
        label="Pareto point",
    )

    ax.set_xlim(doe.bounds[0])
    ax.set_ylim(doe.bounds[1])
    ax.set_zlim(doe.bounds[2])
    ax.set_xlabel(INFO["pfac"].label_with_unit)
    ax.set_ylabel(INFO["temp"].label_with_unit)
    ax.set_zlabel(INFO["lsr"].label_with_unit)
    ax.view_init(19, 60, 0)

    ax.locator_params(nbins=4, axis="x")
    prettify_3d(ax=ax)
    ax.legend()

    if set(targets) != {"ael", "carbs"}:
        plt.subplots_adjust(bottom=0.05, top=0.95, left=0.0, right=0.80)
    else:
        plt.subplots_adjust(bottom=0.05, top=0.95, left=0.05, right=0.95)
        # plt.subplots_adjust(bottom=-0.8, top=1.9, left=0.1, right=0.95)
    fig2 = fig
    ax2 = ax
    plt_objs1 = PlotObjects(fig=fig1, ax=ax1)
    plt_objs2 = PlotObjects(fig=fig2, ax=ax2)
    return plt_objs1, plt_objs2

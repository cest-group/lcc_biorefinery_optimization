from __future__ import annotations

from collections.abc import Sequence
from dataclasses import dataclass
from typing import Callable

import numpy as np
import numpy.typing as npt

from lccopt.doe import DOE
from lccopt.utils import Mesher


def calc_pareto(
    funcs: Sequence[Callable],
    bounds: Sequence | npt.NDArray,
    maximize: npt.ArrayLike = True,
    grid_pts: npt.ArrayLike = 30,
) -> tuple[npt.NDArray, npt.NDArray, npt.NDArray]:
    """Performs a Pareto analysis for a given list of functions.

    The feasible points and Pareto front are calculated on a grid and returned
    along with the corresponding x-points in input space corresponding to the
    Pareto front.

    Parameters
    ----------
    funcs : array_like of functions
        Objective functions for which to perform the Pareto analysis, must take a single
        2D array as input and be vectorized.
    bounds : array_like of float
        The input space bounds.
    maximize : array_like of bool
    Array with same length as funcs where maximize[i] is true if the i:th function
        should be maximized and vice versa.
    grid_pts : array_like of int
        The number of points in each dimension used for the grid.

    Returns
    -------
    X_pareto : np.ndarray
        Input space points corresponding to the Pareto front.
    Y_pareto : np.ndarray
        Pareto front, objective values are stored column-wise.
    Y_feasible : np.ndarray
        Feasible points, including the pareto front. Objective values
        are stored column-wise.
    """
    grid_pts = np.atleast_1d(grid_pts).astype(int)
    if len(grid_pts) == 1:
        grid_pts = np.repeat(grid_pts, len(bounds)).astype(int)

    maximize = np.atleast_1d(maximize).astype(int)
    if len(maximize) == 1:
        maximize = np.repeat(maximize, len(funcs)).astype(bool)

    # flip signs on functions that should be minimized
    funcs_new = []
    for i, maxi in enumerate(maximize):
        if not maxi:
            funcs_new.append(lambda X: -1 * funcs[i](X))
        else:
            funcs_new.append(funcs[i])

    Y_feasible = np.zeros((np.product(grid_pts), len(funcs)))
    mesher = Mesher(bounds, grid_pts=grid_pts)
    for i, f in enumerate(funcs_new):
        Y_feasible[:, i] = mesher.calc_func(f, coord_shaped=True)

    pareto_mask = np.ones(Y_feasible.shape[0], dtype=bool)
    for i, y in enumerate(Y_feasible):
        if np.any(np.all(y < Y_feasible, axis=1)):
            pareto_mask[i] = False

    Y_pareto = Y_feasible[pareto_mask]
    X_pareto = mesher.coords[pareto_mask]

    # flip signs back
    for i, maxi in enumerate(maximize):
        if not maxi:
            Y_pareto[:, i] *= -1
            Y_feasible[:, i] *= -1

    return X_pareto, Y_pareto, Y_feasible


@dataclass
class MetricData:
    X: np.ndarray
    y_pred: np.ndarray
    y_true: np.ndarray
    y_std: np.ndarray
    mse: float
    rmse: float
    mae: float
    mape: float
    gp_std: float
    gp_stdr: float
    mre: float
    samples: np.ndarray

    @property
    def n_samples(self) -> int:
        return len(self.y_pred)

    def summarize(self):
        print("Range-relative errors")
        print("---------------------")
        print(f"MRE: {self.mre:.2f}%")
        print(f"GP RSTD: {self.gp_stdr:.2f}%\n")

        print("Standard metrics")
        print("----------------")
        print(f"MAE: {self.mae:.2f}")
        print(f"MAPE: {self.mape:.2f}%")
        print(f"RMSE: {self.rmse:.2f}\n")


def calc_score(
    target: str,
    doe: DOE | None = None,
    dataset: str = "test",
    max_batch: int | str = 7,
) -> MetricData:
    doe = doe or DOE()
    model = doe.fit_model(target, max_batch=max_batch)
    if dataset == "train":
        df_fil, mask = doe._filter_dataframe(target, max_batch=max_batch)
        X = df_fil.loc[mask, doe.inputs].to_numpy()
        y_train = np.squeeze(df_fil.loc[mask, target].to_numpy())
        y_true = y_train
        # X_train = model.select("X").copy()
        # y_train = np.squeeze(model.select("Y"))
        # y_true = -y_train  # flip sign back
        samples = df_fil.index[mask].to_numpy()
    else:
        df_test = doe.df_main[doe.df_main["target"] == "test"]
        mask = ~df_test[target].isna()
        X = df_test.loc[mask, doe.inputs].to_numpy()
        y_true = np.squeeze(df_test.loc[mask, target].to_numpy())
        samples = df_test.index[mask].to_numpy()

    y_pred, y_var = model.predict(X, with_var=True)

    n_data = len(y_true)
    y_std = np.sqrt(y_var)
    mse = np.sum((y_pred - y_true) ** 2) / n_data
    mape = 100 * np.mean(np.abs((y_pred - y_true) / y_true))

    yrange = np.diff(model.results.get_est_yrange()).item()
    mre = 100 * np.mean(np.abs(y_pred - y_true)) / yrange
    gp_std = np.sqrt(np.sum(y_std**2) / n_data)

    metrics = MetricData(
        X=X,
        y_pred=y_pred,
        y_true=y_true,
        y_std=y_std,
        mse=mse,
        rmse=np.sqrt(mse),
        mae=np.mean(np.abs(y_pred - y_true)),
        mre=mre,
        mape=mape,
        gp_std=gp_std,
        gp_stdr=100 * gp_std / yrange,
        samples=samples,
    )
    return metrics

from __future__ import annotations

from dataclasses import dataclass



@dataclass
class VarInfo:
    name: str
    label: str  # label that would be used in a plot
    noise: list | None = None  #  array of noises used in each batch
    unit: str = ""

    @property
    def label_with_unit(self):
        if self.unit:
            return r"%s (%s)" % (self.label, self.unit)
        else:
            return r"%s" % (self.label)

INFO = {
    # inputs
    "pfac": VarInfo(
        name="pfac",
        label=r"P-factor",
    ),
    "temp": VarInfo(
        name="temp",
        label=r"Temperature",
        unit=r"$_{}^{\circ}\mathrm{C}$",
    ),
    "lsr": VarInfo(
        name="lsr",
        label=r"Liquid-to-solid ratio",
    ),
    # outputs
    "ael": VarInfo(
        name="ael",
        noise=[2.09] * 6 + [1.99] + [1.36],
        label=r"Lignin yield",
        unit=r"%",
    ),
    "carbs": VarInfo(
        name="carbs",
        noise= [2.25] * 6 + [25.85] + [29.52],
        label=r"Carbohydrates",
        unit=r"per $100$ Ar",
    ),
    "bo4": VarInfo(
        name="bo4",
        noise=[5.2] * 8,
        label=r"$\beta$-O-4",
        unit=r"per $100$ Ar",
    ),
    "rsi": VarInfo(
        name="rsi",
        noise=[0.16] * 8,
        label=r"nRSI",
        unit=r"$\mathrm{mmolg}^{-1}$",
    ),
    "chain": VarInfo(
        name="chain",
        noise=[1.23] * 8,
        label=r"Carbohydrate chain length",
        unit=r"",
    ),
    "tg": VarInfo(
        name="tg",
        noise=[52.28] * 8,
        label=r"$\mathrm{T}_g$",
        unit=r"$_{}^{\circ}\mathrm{C}$",
    ),
    "surf025": VarInfo(
        name="surf025",
        noise=[2.86] * 8,
        label=r"Surface tension",
        unit=r"$\mathrm{mNm}^{-1}$"
    ),
    "surf04": VarInfo(
        name="surf04",
        noise=[4.03] * 8,
        label=r"Surface tension",
        unit=r"$\mathrm{mNm}^{-1}$"
    ),
    "surf05": VarInfo(
        name="surf05",
        noise=[3.06] * 8,
        label=r"Surface tension",
        unit=r"$\mathrm{mNm}^{-1}$"
    ),
    "surf01": VarInfo(
        name="surf01",
        noise=[1.96] * 8,
        label=r"Surface tension",
        unit=r"$\mathrm{mNm}^{-1}$"
    ),
    "surf008": VarInfo(
        name="surf008",
        noise=[2.13] * 8,
        label=r"Surface tension",
        unit=r"$\mathrm{mNm}^{-1}$"
    ),
}

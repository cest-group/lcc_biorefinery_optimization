from __future__ import annotations

import pickle

import numpy as np
import numpy.typing as npt
import pandas as pd
from boss.bo.bo_main import BOMain
from boss.bo.results import minimize_model
from scipy.optimize import OptimizeResult

from lccopt.data import INFO
from lccopt.utils import get_git_root


def f_dummy(X):
    raise ValueError("I must not be called.")


class Model:
    def __init__(self, bounds: npt.ArrayLike, maximize: bool = True) -> None:
        """
        Wrapper class for BOSS models.

        Parameters
        ----------
        bounds : np.ndarray
            Bounds for the problem.

        maximize : bool
            If True we model the negative of the objective
            so that minimization yields a maximum.

        """
        self.bounds = np.atleast_2d(bounds)
        self.bo = None
        self.results = None
        self.y_sign = 1
        if maximize:
            self.y_sign = -1

    @property
    def maximize(self):
        return self.y_sign == -1

    @property
    def X(self) -> np.ndarray | None:
        if self.bo is not None:
            return np.squeeze(self.results.select("X"))
        else:
            return None

    @property
    def Y(self) -> np.ndarray | None:
        if self.bo is not None:
            return -np.squeeze(self.results.select("Y"))
        else:
            return None

    def fit(self, X: np.ndarray, Y: np.ndarray, **boss_kwargs):
        """Fit a BO model to given (X, y) data.

        Parameters
        ----------
        X : np.ndarray
            A 2D array where each row represents a feature point.
        y : np.ndarray
            A 1D array of y-values (property values) corresponding to X.
        noise : float
            The noise parameter passed to BOSS.
        boss_kwargs : kwargs
            Additional keyword arguments passed to BOSS.
        """
        Y *= self.y_sign

        yrange = [np.min(Y), np.max(Y)]

        # Set default keywords for the BO model & overwrite with keywords passed.
        kws = dict(
            initpts=0,
            iterpts=0,
            yrange=yrange,
            kernel="rbf",
        )
        kws.update(boss_kwargs)

        self.bo = BOMain(f_dummy, self.bounds, **kws)
        self.bo.init_run(X, Y)
        self.results = self.bo.results

    def predict(
        self, X: np.ndarray, with_var: bool = False
    ) -> np.ndarray | tuple[np.ndarray, np.ndarray]:
        pred = self.bo.model.predict(X)
        y_pred = np.squeeze(self.y_sign * pred[0])
        y_var = np.squeeze(pred[1])
        if with_var:
            return y_pred, y_var
        else:
            return y_pred

    def select(self, *args, **kwargs):
        return self.results.select(*args, **kwargs)

    def get_all_params(self) -> dict:
        return self.bo.model.get_all_params()

    def get_unfixed_params(self) -> dict:
        return self.bo.model.get_unfixed_params()

    def optimize(self) -> OptimizeResult:
        x, fun, _ = minimize_model(self.bo.model, self.bounds)
        fun *= self.y_sign
        ret = OptimizeResult(x=x, fun=fun)
        return ret


class DOE:
    def __init__(self):
        self.df_main = pd.read_csv(
            get_git_root(__file__) / "data/data_master.csv",
            index_col="ID",
        )
        self.bounds = np.array(
            [[250.0, 1000.0], [160.0, 195.0], [0.25, 2.0]], dtype=float
        )
        self.inputs = ("pfac", "temp", "lsr")

    def get_noise(self, target: str, max_batch: int | str) -> float:
        noise = INFO[target].noise[max_batch]
        return noise

    def _filter_dataframe(self, target: str, max_batch: int | str = 7):
        batches = np.arange(0, max_batch + 1)
        df_fil = self.df_main[self.df_main["batch"].isin(batches)]
        mask = ~df_fil[target].isna()
        return df_fil, mask

    def fit_model(
        self,
        target: str,
        max_batch: int | str = 7,
        maximize: bool = True,
        use_cache: bool = False,
        write_cache: bool = False,
        **boss_kwargs,
    ) -> Model:
        # load cached model if it exists and we use no extra BOSS settings
        git_root = get_git_root(__file__)
        path_cache = git_root / "models"
        if use_cache and len(boss_kwargs) == 0:
            file_cache = path_cache / f"model-{target}_batch-{max_batch}.pkl"
            with open(file_cache, "rb") as fp:
                model = pickle.load(fp)
                print(f"Loaded cached model from: {file_cache}")
                return model

        df_curr, mask = self._filter_dataframe(target, max_batch=max_batch)
        X = df_curr.loc[mask, self.inputs].to_numpy()
        Y = df_curr.loc[mask, target].to_numpy()

        noise_default = self.get_noise(target, max_batch)
        noise = boss_kwargs.get("noise", noise_default)
        model = Model(bounds=self.bounds, maximize=maximize)
        model.fit(
            X,
            Y,
            noise=noise,
            **boss_kwargs,
        )
        # cache newly fit models without extra settings
        if len(boss_kwargs) == 0:
            file_cache = path_cache / f"model-{target}_batch-{max_batch}.pkl"
            if write_cache:
                with open(file_cache, "wb") as fp:
                    pickle.dump(model, fp)
                    print(f"Cached model in: {file_cache}")
        return model
